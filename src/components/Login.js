import React, { useState } from 'react';
import axios from 'axios';
import './Login.css';
import { Redirect } from 'react-router-dom';

export default function Login() {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    async function loginUser(credentials) {
        return await axios.post('http://localhost:4000/api/login', { ...credentials }, {
            headers: { 'Content-Type': 'application/json' }
        }).then(data => data.data).catch(err => err.response.data.message);
    }
    
    const submitLogin = async e => {
        e.preventDefault();
        const token = await loginUser({
            username,
            password
        });
       token ? history.push("/cities") : null
    }

    return (
        <div className="loginWrapper">
            <div className="card">
                <h4>Login</h4>
                <form onSubmit={submitLogin}>
                    <div className="inputWrapper">
                        <label>User Name</label>
                        <input type="text" placeholder="Enter Your UserName" name="username" onChange={e => setUserName(e.target.value)} />
                    </div>
                    <div className="inputWrapper">
                        <label>Password</label>
                        <input type="password" placeholder="Enter Your Password" name="pass" onChange={e => setPassword(e.target.value)} />
                    </div>
                    <button type="submit" className="btn">Login</button>
                </form>
            </div>
        </div>
    )
}