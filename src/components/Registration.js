import React, { useState } from 'react';
import axios from 'axios';
import './Registration.css';
export default function Registration() {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    async function registerUser(credentials) {
        return await axios.post('http://localhost:4000/api/register', { ...credentials }, {
            headers: { 'Content-Type': 'application/json' }
        }).then(data => data.data).catch(err => err.response.data.message)
    }

    const submitRegistration = async e => {
        e.preventDefault();
        const data = await registerUser({
            username,
            password
        });
        console.log(data);
    }
    return (
        <div className="registerWrapper">
            <div className="card">
                <h4>Register</h4>
                <form onSubmit={submitRegistration}>
                    <div className="inputWrapper">
                        <label>User Name</label>
                        <input type="text" placeholder="Enter Your UserName" name="username" onChange={e => setUserName(e.target.value)} />
                    </div>
                    <div className="inputWrapper">
                        <label>Password</label>
                        <input type="password" placeholder="Enter Your Password" name="pass" onChange={e => setPassword(e.target.value)} />
                    </div>
                    <button type="submit" className="btn">Register</button>
                </form>
            </div>
        </div>
    )
}