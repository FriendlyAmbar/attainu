import React, { useState, useEffect } from 'react';

export default function Cities() {
    const [cities, setCities] = useState([]);
    const mystyle = {
        width: "100%",
        padding: "10px"
    };
    const cityStyle = {
        minWidth: "30%",
        margin: "2px 0px",
        display: "inline-block"
    }
    useEffect(() => {
        fetch('./cities.json', {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (myJson) {
                setCities(myJson)
            });
    }, []);
    return (
        <div className="citiesWrapper" style={mystyle}>
            {cities.map((city, i) => {
                return <p key={i} style={cityStyle}>Name: {city.name}, State: {city.state}</p>
            })}
        </div>
    )
}