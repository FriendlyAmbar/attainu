import React, { useState, useEffect } from 'react';
import axios from 'axios';


export default function Cities() {
    const [cities, setCities] = useState({});
    useEffect(() => {
        fetch('./cities.json', {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
            .then(function (response) {
                console.log(response)
                return response.json();
            })
            .then(function (myJson) {
                console.log(myJson);
                setCities(myJson)
            });
    }, []);

    return (
        <div className="citiesWrapper">
            {/* {cities} */}
        </div>
    )
}