import React, { useState  } from 'react';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import './App.css';
import Cities from './components/Cities';
import Login from './components/Login';
import Registration from './components/Registration';

function App() {
  return (
    <div className="wrapper">
      <BrowserRouter>
        <nav>
          <ul>
            <li><Link to="/login">Login</Link></li>
            <li><Link to="/register">Register</Link></li>
            <li><Link to="/cities">Cities</Link></li>
          </ul>
        </nav>
        <Switch>
          <Route path="/login">
            <Login></Login>
          </Route>
          <Route path="/register">
            <Registration></Registration>
          </Route>
          <Route path="/cities">
            <Cities></Cities>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
